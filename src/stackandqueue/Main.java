/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stackandqueue;


public class Main {

    
    static QueueInterface quque;
    static StackInterface stack1;
    
    
    
    static FixedStack stack;
   
    static DynamicStack dynamicStack;
    
    static FixedQueue  fixedQueue;
    
    static DynamicQueue dynamicQueue;

    public static void main(String[] args) {

        
          /*********************************************************************************************/
        /* dynamicStack Queue*/        

  
     dynamicQueue=new DynamicQueue();
    quque=dynamicQueue;
 //  quque.Enque("Good");
//   quque.Deque();

/*Enque method test*/
/*
         System.out.println("");
        System.out.println("Size of dynamicQueue:  " + dynamicQueue.size());
        dynamicQueue.Enque("Good");
        dynamicQueue.Enque("Hello");
        dynamicQueue.Enque("Yes");
           System.out.println("Size of dynamicQueue:  " + dynamicQueue.size());
        dynamicQueue.Enque("Hi");
        dynamicQueue.Enque("And");
        System.out.println("Size of dynamicQueue:  " + dynamicQueue.size());
        System.out.println("");
*/
/* Deque method test*/

/*
        dynamicQueue.Enque("Good");
        dynamicQueue.Enque("Hello");
        System.out.println("Size after calling Enque is " + dynamicQueue.size());
        dynamicQueue.Enque("Yes");
        dynamicQueue.Enque("Hi");
        dynamicQueue.Enque("And");
        System.out.println("Deque from top of ququ " + dynamicQueue.Deque());
        System.out.println("Size after calling Deque is " + dynamicQueue.size());
        System.out.println("Deque from top of ququ " + dynamicQueue.Deque());
        System.out.println("Size after calling Deque is " + dynamicQueue.size());
        System.out.println("Deque from top of ququ " + dynamicQueue.Deque());
        System.out.println("Size after calling Deque is " + dynamicQueue.size());
*/
    
        dynamicQueue.Enque("Good");
        dynamicQueue.Enque("Hello");
        System.out.println("Size after calling Enque is " + dynamicQueue.size());
        dynamicQueue.Enque("Yes");
        dynamicQueue.Enque("Hi");
        dynamicQueue.Enque("And");

        System.out.println("peek : " + dynamicQueue.peek());
        System.out.println("Enququ element : " + dynamicQueue.Deque());
        System.out.println("peek : " + dynamicQueue.peek());
        System.out.println("Enququ element : " + dynamicQueue.Deque());
         System.out.println("peek : " + dynamicQueue.peek());
        System.out.println("Enququ element : " + dynamicQueue.Deque());




        
        /*********************************************************************************************/
        /* Fixed Queue*/
          fixedQueue= new FixedQueue();
                
        quque=fixedQueue;
       // quque.Enque("Good");
      
        /*size() method test */
        /*
        System.out.println("");
        System.out.println("Size of fixedQueue:  " + fixedQueue.size());
        fixedQueue.Enque("Good");
        System.out.println("Size of fixedQueue after Enque:  " + fixedQueue.size());
        fixedQueue.Deque();
        System.out.println("Size of fixedQueue after Deque: " + fixedQueue.size());
        System.out.println("");
        */

        
        /*Enque method test */
       /*
        fixedQueue.Enque("Good");
        fixedQueue.Enque("Hello");
        fixedQueue.Enque("Yes");  
        fixedQueue.Enque("Hi");
        fixedQueue.Enque("And");
        */
           
       /*Deque method test */
       /*
         fixedQueue.Enque("Good");
        fixedQueue.Enque("Hello");
        fixedQueue.Enque("Yes"); 
         fixedQueue.Enque("Hi");
         fixedQueue.Enque("And");
       
     
              
          System.out.println("Deque from top of ququ "+fixedQueue.Deque());
          System.out.println("Size after calling Deque is "+fixedQueue.size());
          System.out.println("Deque from top of ququ "+fixedQueue.Deque());
          System.out.println("Size after calling Deque is "+fixedQueue.size());
          System.out.println("Deque from top of ququ "+fixedQueue.Deque());
          System.out.println("Size after calling Deque is "+fixedQueue.size());
          fixedQueue.Enque("try this");
          System.out.println("Size after calling Enque is "+fixedQueue.size());
        //  System.out.println("Deque from top of ququ "+fixedQueue.Deque());
        //  System.out.println("Size after calling Deque is "+fixedQueue.size());
        */
        
          /*
          
          /*peek test method*/
          /*
            System.out.println(" //");
        System.out.println(" ");
          System.out.println("     peek : " + fixedQueue.peek());
         System.out.println("Deque from top of ququ "+fixedQueue.Deque());
          System.out.println("Size after calling Deque is "+fixedQueue.size());
        System.out.println("     peek : " + fixedQueue.peek());
        System.out.println("Deque element : " + fixedQueue.Deque());
         System.out.println("     peek : " + fixedQueue.peek());
        System.out.println("Deque element : " + fixedQueue.Deque());
        */
        /*********************************************************************************************/
        /* Dynamic Stack */
       
         dynamicStack = new DynamicStack();
           /* Push method */
       /*
        
       dynamicStack.push("Good");
     
       System.out.println("Size after push is "+dynamicStack.size());
      dynamicStack.push("Hello");
      System.out.println("Size after push is "+dynamicStack.size());
        dynamicStack.push("Yes");
      System.out.println("Size after push is "+dynamicStack.size());
      dynamicStack.push("Hi");
       System.out.println("Size after push is "+dynamicStack.size());
*/
  
        
        
       /*pop method test */
     /*
         System.out.println("pop element from top of stack "+dynamicStack.pop());
          System.out.println("Size after calling pop is "+dynamicStack.size());
          
          System.out.println("pop element from top of stack "+dynamicStack.pop());
          System.out.println("Size after calling pop is "+dynamicStack.size());
          System.out.println("pop element from top of stack "+dynamicStack.pop());
          System.out.println("Size after calling pop is "+dynamicStack.size());
          System.out.println("pop element from top of stack "+dynamicStack.pop());
          System.out.println("Size after calling pop is "+dynamicStack.size());
         */
         // System.out.println("pop element from top of stack "+dynamicStack.pop());
         // System.out.println("Size after calling pop is "+dynamicStack.size());
          
       
      
      /*
          //peek method test
      
        System.out.println(" ");
        System.out.println(" ");
        System.out.println("     peek : " + dynamicStack.peek());
        System.out.println("pop element : " + dynamicStack.pop());
         System.out.println("     peek : " + dynamicStack.peek());
        System.out.println("pop element : " + dynamicStack.pop());
         System.out.println("     peek : " + dynamicStack.peek());
        System.out.println("pop element : " + dynamicStack.pop());
          System.out.println("     peek : " + dynamicStack.peek());
     //   System.out.println("pop element : " + dynamicStack.pop());
           System.out.println("     peek : " + dynamicStack.peek());
      //  System.out.println("pop element : " + dynamicStack.pop());

  
        
        */
        
        
        
        
        
        
        
     
        
        /*********************************************************************************************/   
        
  
        
        /* Fixed length Stack */
    //stack = new FixedStack();
   

           
       //push method test
    
     //stack.push("Good");
     //   stack.push("Hello");

      //  stack.push("Yes");
      //  stack.push("hi");
   //  stack.push("Good");
    //  stack.push("Hello");
  
  // stack.push("Yes");
  //  stack.push("hi");
      


       
       /*
        //pop method test 
      
          System.out.println("pop element from top of stack "+stack.pop());
          System.out.println("Size after calling pop is "+stack.size());
        //  System.out.println("pop element from top of stack "+stack.pop());
         // System.out.println("Size after calling pop is "+stack.size());
        //      System.out.println("pop element from top of stack "+stack.pop());
       //   System.out.println("Size after calling pop is "+stack.size());
  
       
       */
       
       
       
 
      //peek method test
       /*
        System.out.println("     peek : " + stack.peek());
        System.out.println("pop element : " + stack.pop());

        System.out.println("     peek : " + stack.peek());
        System.out.println("pop element : " + stack.pop());

        System.out.println("     peek : " + stack.peek());
        System.out.println("pop element : " + stack.pop());
        System.out.println("     peek : " + stack.peek());
        System.out.println("pop element : " + stack.pop());
 */
 
        

          //Size method test
          /*
        System.out.println("Size of crrent stack is " + stack.size());

        System.out.println("pop element from top of stack " + stack.pop());
        System.out.println("Size of crrent stack  after calling pop is " + stack.size());
        System.out.println("pop element from top of stack " + stack.pop());
        System.out.println("Size of crrent stack  after calling pop is " + stack.size());
        System.out.println("pop element from top of stack " + stack.pop());
        System.out.println("Size of crrent stack  after calling pop is " + stack.size());
      */
 /*
       //when count is = 0 ; then need to handle Exception
           System.out.println("pop element from top of stack "+stack.pop());
      System.out.println("Size of crrent stack  after calling pop is "+stack.size());

         */
    }
}
