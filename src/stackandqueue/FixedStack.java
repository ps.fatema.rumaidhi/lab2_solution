/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stackandqueue;




public class FixedStack implements StackInterface{
    

    static String stack[];
    static int count;

    public FixedStack() {
      stack= new String[5]; 
        // stack=new String[]{"Good","Hello","Yes","Hi"};
        count=0;
    }
 
    
    
    //size effected by push and pop
      public int size() {

        return count;
    }

    public void push(String element) throws IllegalArgumentException{
        
        
          
          if (count ==stack.length){
               throw new IllegalArgumentException("Stack is Full");
          }
          else{
            count++;  
          }
          
         if(count-1 ==0){
          stack[0]=element;
         }
         else{
            for(int index=count;index>0;index--){
                stack[index]=stack[index-1];
            }
             stack[0]=element;
         }
       
      
       
    }
    
    
    
    
    
    

    public String pop()throws IndexOutOfBoundsException{
         
          if (count ==0){
               throw new IndexOutOfBoundsException("Stack is empty");
          }
        else {
            count--; //decrease the size
        }

        String element = stack[0];
        
        for (int i = 0; i < count + 1; i++) {
            stack[i] = stack[i + 1];
        }
        
    
        return element;
    }

    public String peek() throws IndexOutOfBoundsException{
                if (count == 0) {
            throw new IndexOutOfBoundsException("Stack is empty");
        }
        
     
        return stack[0];
    }

  
}
