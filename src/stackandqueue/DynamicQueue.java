/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stackandqueue;

import static stackandqueue.FixedQueue.count;


public class DynamicQueue implements QueueInterface{
    
    static String first[];
    static String second[];
    static int count = 0;
  
    public DynamicQueue (){
         first = new String[count];
    }
    public int size() {
  return first.length;
    }

    public void Enque(String element) {
        count++;
         second = new String[count];
       
         //copy from first to second
         for(int index=0;index<first.length;index++){
             second[index]=first[index];
         }
           second[count-1]=element;
           //copy second to first
           
           first=new String[count];
              for(int index=0;index<second.length;index++){
             first[index]=second[index];
       
         }
         
         
              
         System.out.println("------------");

        for (int i = 0; i < first.length; i++) {
            System.out.println("first [" + i + "] =" + first[i]);
        }
        
         System.out.println("------------");
           
         
    }

    
      public String Deque() throws IndexOutOfBoundsException{
             if (count == 0) {
            throw new IndexOutOfBoundsException("Queue is empty");
        }
     
         String  element = first[0];
         count--;
         second=new String[count];
         //copy first to second without first index's element
        for (int index = 1; index < first.length; index++) {
           second[index-1]=first[index];
        }
        
        first=new String [count];
        //copy second to first
          for (int index = 0; index < second.length; index++) {
                 first[index]=second[index];
          }
     
           return element;
    }
      
      
    public String peek() throws IndexOutOfBoundsException{
             if (count == 0) {
            throw new IndexOutOfBoundsException("Queue is empty");
        }
 
        return first[0];

    }

  
    
}
